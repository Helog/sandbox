var timer = {};

(function(global) {
	
	var startTime = null;
	var stopTime = null;
	
	var globalTime = 0;
	var accuTime = 0;
	
	global.startTimer = function() {
		startTime = new Date();
	};
	
	global.updateTime = function() {
		stopTime = new Date();
		accuTime = stopTime - startTime;
	}
	
	global.accumulateTime = function() {
		stopTime = new Date();
		accuTime = stopTime - startTime;
		globalTime += accuTime;
		accuTime = 0;
	};
	
	global.resetTime = function() {
		startTime = null;
		stopTime = null;
		globalTime = 0;
	}
	
	global.getGlobalTime = function() {
		return globalTime + accuTime;
	};
	
})(timer);