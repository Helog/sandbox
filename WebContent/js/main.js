window.onload = function() {
	
	console.log("hello");
	
	var interval = null;

	$("#timerButton").mousedown(function() {
		timer.startTimer();
		if (interval == null) {
			interval = setInterval(function(){ updateTime() }, 100);
		}
	});
	
	$("#timerButton").mouseup(function() {
		timer.accumulateTime();
		displayTime();
		clearInterval(interval);
		interval = null;
	});
	
	$("#resetButton").click(function() {
		timer.resetTime();
		displayTime();
	});

	var displayTime = function() {
		var time = timer.getGlobalTime();
		var seconds = Math.floor(time / 1000);
		var milliSeconds = time % 1000;
		var timeDisplay = seconds + "." + milliSeconds + " s";
		$("#displayTime").html(timeDisplay);
	};
	
	var updateTime = function() {
		timer.updateTime();
		displayTime();
	};

	displayTime();
	
};