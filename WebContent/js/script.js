
function on_connect(status) {
	if (status === Strophe.Status.CONNECTED) {
		$('#notifications').html('CONNECTED');
		$("#login_button").button("disable");
		$("#logout_button").button("enable");
		xmpp.init_connection(on_message, on_presence, on_roster);
	} else if (status === Strophe.Status.DISCONNECTED) {
		$('#notifications').html('DISCONNECTED');
		$("#login_button").button("enable");
		$("#logout_button").button("disable");
		$("#login_dialog").dialog("open");
	} else if (status === Strophe.Status.ERROR) {
		$('#notifications').html('ERROR');
	} else if (status === Strophe.Status.CONNECTING) {
		$('#notifications').html('CONNECTING');
	} else if (status === Strophe.Status.CONNFAIL) {
		$('#notifications').html('CONNFAIL');
	} else if (status === Strophe.Status.AUTHENTICATING) {
		$('#notifications').html('AUTHENTICATING');
	} else if (status === Strophe.Status.AUTHFAIL) {
		$('#notifications').html('AUTHFAIL');
	} else if (status === Strophe.Status.ATTACHED) {
		$('#notifications').html('ATTACHED');
	}
}

function xmpp_connect(event) {
	var username = $("#jid").val();
	var password = $("#jid_pssw").val();
	$('#jid_pssw').val('');

	xmpp.connect(username, password, on_connect);

	$(this).dialog('close');
}

function on_roster(iq) {
	$(iq).find('item').each(
		function() {
			var jid = $(this).attr('jid');
			var name = $(this).attr('name') || jid;

			var contact = "<li id='" + jid + "'>"
					+ "<div class='roster-contact offline'>"
					+ "<div class='roster-name'>" + name + "</div>";
			var button = $('<button/>', {
				text : "'Hello' to " + name,
				id : 'btn_' + name,
				click : message_roster,
				value : jid
			});

			$("#friends").append(contact);
			$("#friends").append(button);
			$("#friends").append("</div></div></li>");
		}
	);
}

function on_message(message) {
	if (xmpp.is_chat_state_message(message)) {
		$("#chat_state").html(xmpp.get_chat_state_message(message));
	} else if (xmpp.is_mood_message(message)) {
		var from = xmpp.get_from_message(message);
		var text = xmpp.get_text_message(message);
		var mood = xmpp.get_mood_message(message);
		var time = xmpp.get_time_message(message);
		
		var mess = "[" + mood + "](" + time + "s) " + text;
		var display_message = $("<p></p>").text("Message from " + from + " : " + mess);
		$("#message").append(display_message);
	}

	console.log(message);
	return true;
}

function on_presence(presence) {
	var full_jid = $(message).attr('from');
	console.log("Presence ");
	console.log(presence);
	
	return true;
}

function message_roster() {
	var dest = $(this).val();
	dest = Strophe.getNodeFromJid(dest);
	
	$("#messaging_dialog").dialog("option", "buttons", 
	  [{
			text: "Send",
	      click: function() {
			send_message(dest);
	      }
	    }]
	);
	$("#messaging_dialog").dialog("option", "title", "Message to " + dest);

	$('#messaging_dialog').dialog("open");
}

function send_message(dest) {
	xmpp.send_mood_message(dest, $("#msg_text").val(), 
			$("#msg_mood option:selected").text(), $("#msg_time").val());
	
	$('#messaging_dialog').dialog("close");
}

function logout() {
	xmpp.disconnect(null);

	$("#friends").empty();
	$("#message").empty();
	$("#logout_button").button("disable");
}

function login() {
	$("#login_dialog").dialog("open");
}

$(document).ready(function() {

	$('#login_dialog').dialog({
		autoOpen : true,
		draggable : true,
		modal : true,
		title : 'Connect to XMPP',
		buttons : {
			"Connect" : xmpp_connect
		}
	});

	$('#messaging_dialog').dialog({
		autoOpen : false,
		draggable : true,
		modal : true,
		buttons : [{
			text: "Send"
		}]
	});

	$("#login_button").button({
		label: "Login"
	});
	$("#login_button").click(login);
	
	$("#logout_button").button({
		label: "Logout",
		disabled: true
	});
	$("#logout_button").click(logout);

});
