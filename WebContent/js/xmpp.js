var xmpp = {};

(function(global) {
	
	var connection = null;
	
	/**
	 * @param username [String] Jid
	 * @param password [String] Xmmp account password
	 * @param on_connect [function(status)] Callback triggered  with connection
	 * status events
	 */
	global.connect = function(username, password, on_connect) {
		connection = new Strophe.Connection(BOSH_SERVICE);
		connection.connect(username + "@" + DOMAIN, password, on_connect);
	};
	
	global.disconnect = function(reason) {
		if (reason != null) {
			connection.disconnect(reason);
		} else {
			connection.disconnect();
		}
		connection = null;
	};
	
	global.init_connection = function(on_message, on_presence, on_roster) {
		connection.send($pres());

		connection.addHandler(on_message, null, "message", "chat");
		connection.addHandler(on_presence, null, "presence");

		var iq = $iq({
			type : 'get'
		}).c('query', {
			xmlns : 'jabber:iq:roster'
		});
		connection.sendIQ(iq, on_roster);
	};
	
	global.send_mood_message = function(dest_username, msg_text, msg_mood, msg_time) {
		var message = $msg({
			to : dest_username + "@" + DOMAIN,
			"type" : "chat"
		}).c('body').t(msg_text);
		message.up().c("data", {time: msg_time, mood: msg_mood});
		
		connection.send(message);
	};
	
	global.get_from_message = function(message) {
		return $(message).attr('from');
	};
	
	global.is_mood_message = function(message) {
		return $(message).find('body').length > 0;
	};
	
	global.is_chat_state_message = function(message) {
		return !($(message).find('body').length > 0);
	};
	
	global.get_text_message = function(message) {
		if (xmpp.is_mood_message(message)) {
			return $(message).find('body').text();
		}
		console.log("In 'get_mood_message' : the stanza has no mood_message.")
		return null;
	};
	
	global.get_mood_message = function(message) {
		if (xmpp.is_mood_message(message)) {
			var data = $(message).find('data');
			return $(data).attr("mood");
		}
		console.log("In 'get_mood_message' : the stanza has no mood_message.")
		return null;
	};
	
	global.get_time_message = function(message) {
		if (xmpp.is_mood_message(message)) {
			var data = $(message).find('data');
			return $(data).attr("time");
		}
		console.log("In 'get_mood_message' : the stanza has no mood_message.")
		return null;
	};
	
	global.get_chat_state_message = function(message) {
		if ($(message).find('active').length > 0) {
			return "active";
		}
		if ($(message).find('inactive').length > 0) {
			return "inactive";
		}
		if ($(message).find('composing').length > 0) {
			return "composing";
		}
		if ($(message).find('paused').length > 0) {
			return "paused";
		}
		if ($(message).find('gone').length > 0) {
			return "gone";
		}
		
		console.log("In 'get_chat_state_message' : the stanza has no chat state.")
		return null;
	};
	
	
})(xmpp);